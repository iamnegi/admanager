<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AdsController@index');
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Auth::routes();

//Home
Route::get('/home', 'HomeController@index')->name('home');

//Ads
Route::get('/ads-list', 'AdsController@index')->name('ads-list');
Route::PATCH('ads/delete', 'AdsController@update')->name('ads.delete');
Route::get('ads/search', 'AdsController@search')->name('ads.search');

//Notes
Route::post('ads/note', 'NoteController@store')->name('ads.note');
Route::post('ads/updateNote', 'NoteController@updateNote')->name('ads.note.update');

//Category
Route::post('ads/category', 'CategoryController@store')->name('ads.category');
Route::post('ads/assign/category', 'CategoryController@assignCategory')->name('ads.assign.category');