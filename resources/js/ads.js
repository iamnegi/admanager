(function ( $ ) {
    var categoryStore = [];
    
    deleteAds = function (adsId) {
        $("#deleteAdsItem [name='id']").val(adsId)
    }
    addNote = function (adsId) {
        $("#addNoteForm [name='ads_id']").val(adsId)
    }
    
    noteUpdate = function () {
        const freeNoteWrap = $(".free-text-enable").find(".free-note-editor")
        freeNoteWrap.slideUp()
        freeNoteWrap.removeClass('error')

        const textarea = freeNoteWrap.find("textarea")
        const url = window.location.origin + '/ads/updateNote'
        $.post(url, { nid : $(textarea).attr('nid'), note: $(textarea).val() , _token: $("#modalAddNote input[name='_token']").val()})
        .done(function(data) {
            console.log(data)
        })
        .fail(function() {
            alert( "error" );
        })
    }

    addCategory = function(adsId) {
        $("#addCategoryForm [name='ads_id']").val(adsId)
    }
    
    assignCategory = function(adsId) {
        $("#assignCategoryForm [name='ads_id']").val(adsId)
    }

    selectCategorySlot = function() {
        if(categoryStore.length <= 0) {
            $(".ul-category [data-cat='0']").addClass('btn-dark').removeClass('btn-secondary')
            $(".category-btn").addClass('btn-secondary').removeClass('btn-dark')
            return false
        } else {
            $(".ul-category [data-cat='0']").addClass('btn-secondary').removeClass('btn-dark')
            $(".category-btn").addClass('btn-secondary').removeClass('btn-dark')
            categoryStore.map(ids=> {
                $(".ul-category [data-cat='"+ids+"']").addClass('btn-dark').removeClass('btn-secondary')
            });
        } 
    },

    toggleCategoryCard = function() {
        if(categoryStore.length <= 0) {
            $("div.category-card").show()
            return false
        }
        $("div.category-card").each(function() {
            var catString = $(this).attr('data-category')
            if(catString && catString!=='0') {
                catStore = catString.split(',')
                const intersection = categoryStore.filter(element => catStore.includes(element))
                if (intersection.length > 0) {
                    $(this).show()
                } else {
                    $(this).hide()
                }
            } else {
                $(this).hide()
            }
        })
    }
    
    $(".category-btn").on("click",function(){
        var catId = $(this).data('cat');
        catId = catId.toString();
        if(categoryStore.includes(catId)) {
            var index = categoryStore.indexOf(catId);
            if(index!=-1){
                categoryStore.splice(index, 1);
            }
        } else {
            categoryStore.push(catId);
        }
        selectCategorySlot()
        toggleCategoryCard()
    })

    $(".category-all").on("click",function(){
        $(".ul-category [data-cat='0']").addClass('btn-dark').removeClass('btn-secondary')
        $(".category-btn").addClass('btn-secondary').removeClass('btn-dark')
        $("div.category-card").show();
    })

    $(".btn-notes-notify").hover(
        function() {
            if($(".sc-active-wrap.free-text-enable").length > 0) {
                $(".sc-active-wrap").removeClass('free-text-enable')
            }
            $(this).closest('.sc-active-wrap').find(".free-note-editor").slideDown()
            $(this).closest('.sc-active-wrap').addClass('free-text-enable')
        }
    )

    $('body').click(function(evt){    
        if($(evt.target).closest('.free-note-editor').length)
            return;
        
        if($(".free-text-enable").find(".free-note-editor textarea").val().trim().length <= 0) {
            $(".free-text-enable").find(".free-note-editor").addClass('error')
            return false
        }

        noteUpdate();
    });

    // fillFilters = function () {
    //     var urlParams = new URLSearchParams(window.location.search)
    //     if (urlParams.get('cat')) {
    //         const category = urlParams.get('cat');
    //         const categoryText = $(".catgory-menu [data-cat='"+category+"']").text()
    //         $("#search_concept").text(categoryText)
    //     }
    //     if (urlParams.get('s')) {
    //         const searchText = urlParams.get('s')
    //         $("#searchbar").val(searchText)
    //     }
    // }

    // $('#searchbar').keypress(function(event){
    //     var keycode = (event.keyCode ? event.keyCode : event.which)
    //     if(keycode == '13'){
    //         var url = window.location.href
            
    //         var urlParams = new URLSearchParams(window.location.search)
    //         urlParams.delete('s')
            
    //         if(url.indexOf('?') != -1) {
    //             url += '&s=' + $(this).val()
    //         } else {
    //             url += '?s=' + $(this).val()
    //         }
    //         window.location.href = url;
    //     }
    // });

    // fillFilters();

}( jQuery ));