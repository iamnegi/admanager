@extends('layouts.app')

@section('content')
<div class="container" id="ads-layout">
    
    <!-- <div class="row">    
        <div class="col-12">
            <div class="input-group">
                <div class="input-group-btn search-panel">
                    <button type="button" class="btn btn-default dropdown-toggle btn-category" data-toggle="dropdown">
                        <span id="search_concept">Filter by</span> <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu catgory-menu" role="menu">
                        @foreach($categories as $item)
                          <li><a href="{{route('ads.search', ['cat'=>$item->id])}}" data-cat="{{$item->id}}">{{$item->title}}</a></li>
                        @endforeach
                    </ul>
                </div>
                <input 
                  id="searchbar" 
                  type="text" 
                  class="form-control search-bar" 
                  name="searchbar" 
                  placeholder="Press ENTER to Search Ads..."
                />
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
        </div>
    </div> -->
    @if($categories->count())
    <div class="row justify-content-left">
      <ul class="col-12 ul-category">
        <li>
          <button class="btn btn-dark category-all" href="javascript:void(0)" data-cat="0">All</button>
        </li>
        @foreach($categories as $item)
        <li>
          <label for="cat_{{$item->id}}" >
            <!-- <input id="cat_{{$item->id}}" type="checkbox" name="category-select[]" value="{{$item->id}}"> -->
            <button class="btn btn-secondary category-btn" href="javascript:void(0)" data-cat="{{$item->id}}">{{$item->title}}</button>
          </label>
        </li>
        @endforeach
      </ul>
    </div>
    @endif

    <div class="row justify-content-right">
      <div class="col-12 text-right">
        <!--Add Category-->
        <a 
          class="btn btn-success" 
          data-toggle="modal" 
          data-target="#modalAddCategory"
          onclick="addCategory()"
          >
          <i></i> Add Category
        </a>
      </div>
    </div>
    
    @include('inc.messages')
    @if($adsItems->count())
    <div class="row justify-content-left">
        @foreach($adsItems as $item)
        <div 
            class="col-md-4 col-sm-1 col-xs-1 col-lg-4 mt-3 category-card" 
            data-category="{{ App\Helpers\Helper::implodeCategory($item->category) }}"
            >
            <div class="card">
                <div class="card-body">
                  <section class="sc-active-wrap">
                        <div class="c-active"><span></span> Active</div>
                        <div class="c-publish">Started running on {{ date('M d, Y', strtotime($item->created_at)) }}</div>
                        <div class="c-id">{{$item->add_fb_id}}</div>
                        <div class="c-tag">
                          {!! App\Helpers\Helper::fetchCategory($item->category) !!}
                        </div>

                        <div class="item-menu">
                            <button 
                                class="btn btn-link dropdown-toggle" 
                                type="button" 
                                id="dropdownMenuButton" 
                                data-toggle="dropdown" 
                                aria-haspopup="true" 
                                aria-expanded="false">
                            </button>
                            @if($item->notes)
                              <a 
                                  class="badge badge-danger btn-notes-notify" 
                                  type="button" 
                                  >
                                  1
                              </a>
                            @endif
                            
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <!--Delete Ad-->
                                <a 
                                  class="dropdown-item" 
                                  data-toggle="modal" 
                                  data-target="#modalDeleteAd"
                                  onclick="deleteAds({{$item->id}})"
                                  >
                                  <i class="fa-pencil"></i> Delete
                                </a>

                                <!--Add Note-->
                                @if(!$item->notes)
                                <a 
                                  class="dropdown-item" 
                                  data-toggle="modal" 
                                  data-target="#modalAddNote"
                                  onclick="addNote({{$item->id}})"
                                  >
                                  <i></i> Add Note
                                </a>
                                @endif

                                <!--List Note-->
                                <a 
                                  class="dropdown-item" 
                                  data-toggle="modal" 
                                  data-target="#modalListNote"
                                  onclick="noteList({{$item->id}})"
                                  >
                                  <i></i> List Note
                                </a>

                                <!--Assign Category-->
                                <a 
                                  class="dropdown-item" 
                                  data-toggle="modal" 
                                  data-target="#modalAssignCategory"
                                  onclick="assignCategory({{$item->id}})"
                                  >
                                  <i></i> Assign Category
                                </a>
                            </div>
                        </div>

                        @if($item->notes)
                        <div class="free-note-editor">
                          <textarea nid="{{$item->notes->id}}" class="form-control" class="note-free-text">{{$item->notes->title}}</textarea>
                        </div>
                        @endif
                    </section>

                    <section class="sc-desc-wrap">
                        <div class="c-brand">
                            <!-- <a>{{$item->title}}</a> -->
                            <span>Sponsored {{strlen($item->sponser) ? ': '. $item->sponser: ''}}</span>
                        </div>
                        <div class="c-desc"> {!!$item->description!!} </div>
                    </section>

                    @if($item->ad_image && $item->ad_image!=='')
                      <section class="sc-ad-sec-image">
                        <a class="ad-image" href="#">
                          @if ($item->type === 'img')
                          <img src="{{$item->ad_image}}">
                          @else
                          <video  width="100%" height="340" controls>
                            <source src="{{$item->ad_image}}" type="video/mp4">
                          </video>
                          @endif
                        </a>
                      </section>
                    @endif

                    <section class="sc-ad-footer">
                      <a class="footer-a" href="{{$item->camp}}" target="_blank">{!!$item->title!!}</a>
                    </section>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    @endif
</div>

<!--Modal Add Note-->
<div 
    class="modal fade" 
    id="modalAddNote" 
    tabindex="-1" 
    role="dialog" 
    aria-labelledby="addNoteModalCenterTitle" 
    aria-hidden="true"
  >
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add Note</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="addNoteForm" method="post" action="{{ route('ads.note') }}">
            {{csrf_field()}}
            <input type="hidden" name="ads_id" value="">
            <div class="form-group">
              <textarea class="form-control" name="title"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Save Note</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!--Modal List Note-->
<div 
    class="modal fade" 
    id="modalListNote" 
    tabindex="-1" 
    role="dialog" 
    aria-labelledby="listNoteModalCenterTitle" 
    aria-hidden="true"
  >
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">List Note</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div>
        <table class="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Note</th>
              <th>Created</th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!--Modal Add Category-->
<div 
    class="modal fade" 
    id="modalAddCategory" 
    tabindex="-1" 
    role="dialog" 
    aria-labelledby="addCategoryModalCenterTitle" 
    aria-hidden="true"
  >
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="addCategoryForm" method="post" action="{{ route('ads.category') }}">
            {{csrf_field()}}
            <input type="hidden" name="ads_id" value="">
            <div class="form-group">
              <input type="text" class="form-control" name="title"/>
            </div>
            <button type="submit" class="btn btn-primary">Save Category</button>
        </form>
      </div>
    </div>
  </div>
</div>


<!--Assign Category-->
<div 
    class="modal fade" 
    id="modalAssignCategory" 
    tabindex="-1" 
    role="dialog" 
    aria-labelledby="assignCategoryModalCenterTitle" 
    aria-hidden="true"
  >
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Assign Category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="assignCategoryForm" method="post" action="{{ route('ads.assign.category') }}">
            {{csrf_field()}}
            <input type="hidden" name="ads_id" value="">
            <div class="form-group">
              @if($categories->count())
              <select class="form-control" name="category[]" multiple="multiple">
                <option>--Select Category--</option>
                @foreach($categories as $item)
                <option value="{{ $item->id }}">{{ $item->title }}</option>
                @endforeach
              </select>
              @endif
            </div>
            <button type="submit" class="btn btn-primary">Save Category</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!--Modal Delete-->
<div 
    class="modal fade" 
    id="modalDeleteAd" 
    tabindex="-1" 
    role="dialog" 
    aria-labelledby="delModalCenterTitle" 
    aria-hidden="true"
  >
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Warning!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="deleteAdsItem" method="post" action="{{ route('ads.delete') }}">
            {{method_field('PATCH')}} 
            {{csrf_field()}}
            <input type="hidden" name="id" value="">
            <p>Are you sure want to delete this Ad?</p>
            <button data-dismiss="modal" type="button" class="btn btn-dark">Cancel</button>
            <button type="submit" class="btn btn-primary">Yes</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
