/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/ads.js":
/*!*****************************!*\
  !*** ./resources/js/ads.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function ($) {
  var categoryStore = [];

  deleteAds = function deleteAds(adsId) {
    $("#deleteAdsItem [name='id']").val(adsId);
  };

  addNote = function addNote(adsId) {
    $("#addNoteForm [name='ads_id']").val(adsId);
  };

  noteUpdate = function noteUpdate() {
    var freeNoteWrap = $(".free-text-enable").find(".free-note-editor");
    freeNoteWrap.slideUp();
    freeNoteWrap.removeClass('error');
    var textarea = freeNoteWrap.find("textarea");
    var url = window.location.origin + '/ads/updateNote';
    $.post(url, {
      nid: $(textarea).attr('nid'),
      note: $(textarea).val(),
      _token: $("#modalAddNote input[name='_token']").val()
    }).done(function (data) {
      console.log(data);
    }).fail(function () {
      alert("error");
    });
  };

  addCategory = function addCategory(adsId) {
    $("#addCategoryForm [name='ads_id']").val(adsId);
  };

  assignCategory = function assignCategory(adsId) {
    $("#assignCategoryForm [name='ads_id']").val(adsId);
  };

  selectCategorySlot = function selectCategorySlot() {
    if (categoryStore.length <= 0) {
      $(".ul-category [data-cat='0']").addClass('btn-dark').removeClass('btn-secondary');
      $(".category-btn").addClass('btn-secondary').removeClass('btn-dark');
      return false;
    } else {
      $(".ul-category [data-cat='0']").addClass('btn-secondary').removeClass('btn-dark');
      $(".category-btn").addClass('btn-secondary').removeClass('btn-dark');
      categoryStore.map(function (ids) {
        $(".ul-category [data-cat='" + ids + "']").addClass('btn-dark').removeClass('btn-secondary');
      });
    }
  }, toggleCategoryCard = function toggleCategoryCard() {
    if (categoryStore.length <= 0) {
      $("div.category-card").show();
      return false;
    }

    $("div.category-card").each(function () {
      var catString = $(this).attr('data-category');

      if (catString && catString !== '0') {
        catStore = catString.split(',');
        var intersection = categoryStore.filter(function (element) {
          return catStore.includes(element);
        });

        if (intersection.length > 0) {
          $(this).show();
        } else {
          $(this).hide();
        }
      } else {
        $(this).hide();
      }
    });
  };
  $(".category-btn").on("click", function () {
    var catId = $(this).data('cat');
    catId = catId.toString();

    if (categoryStore.includes(catId)) {
      var index = categoryStore.indexOf(catId);

      if (index != -1) {
        categoryStore.splice(index, 1);
      }
    } else {
      categoryStore.push(catId);
    }

    selectCategorySlot();
    toggleCategoryCard();
  });
  $(".category-all").on("click", function () {
    $(".ul-category [data-cat='0']").addClass('btn-dark').removeClass('btn-secondary');
    $(".category-btn").addClass('btn-secondary').removeClass('btn-dark');
    $("div.category-card").show();
  });
  $(".btn-notes-notify").hover(function () {
    if ($(".sc-active-wrap.free-text-enable").length > 0) {
      $(".sc-active-wrap").removeClass('free-text-enable');
    }

    $(this).closest('.sc-active-wrap').find(".free-note-editor").slideDown();
    $(this).closest('.sc-active-wrap').addClass('free-text-enable');
  });
  $('body').click(function (evt) {
    if ($(evt.target).closest('.free-note-editor').length) return;

    if ($(".free-text-enable").find(".free-note-editor textarea").val().trim().length <= 0) {
      $(".free-text-enable").find(".free-note-editor").addClass('error');
      return false;
    }

    noteUpdate();
  }); // fillFilters = function () {
  //     var urlParams = new URLSearchParams(window.location.search)
  //     if (urlParams.get('cat')) {
  //         const category = urlParams.get('cat');
  //         const categoryText = $(".catgory-menu [data-cat='"+category+"']").text()
  //         $("#search_concept").text(categoryText)
  //     }
  //     if (urlParams.get('s')) {
  //         const searchText = urlParams.get('s')
  //         $("#searchbar").val(searchText)
  //     }
  // }
  // $('#searchbar').keypress(function(event){
  //     var keycode = (event.keyCode ? event.keyCode : event.which)
  //     if(keycode == '13'){
  //         var url = window.location.href
  //         var urlParams = new URLSearchParams(window.location.search)
  //         urlParams.delete('s')
  //         if(url.indexOf('?') != -1) {
  //             url += '&s=' + $(this).val()
  //         } else {
  //             url += '?s=' + $(this).val()
  //         }
  //         window.location.href = url;
  //     }
  // });
  // fillFilters();
})(jQuery);

/***/ }),

/***/ 1:
/*!***********************************!*\
  !*** multi ./resources/js/ads.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\xampp\htdocs\api\resources\js\ads.js */"./resources/js/ads.js");


/***/ })

/******/ });