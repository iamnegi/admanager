<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ads', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->integer('user_id')->nullable();
          $table->text('title')->nullable();
          $table->string('type', 20)->nullable();
          $table->longText('description')->nullable();
          $table->text('url')->nullable();
          $table->string('add_fb_id',100)->nullable();
          $table->string('sponser',100)->nullable();
          $table->text('extra')->nullable();
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
