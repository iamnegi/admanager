<?php

namespace App\Http\Controllers;

use App\Ads;
use Illuminate\Http\Request;
use App\Repositories\AdsRepository;

class AdsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $model;
    
    public function __construct(Ads $ads)
    {
        $this->middleware('auth');
        $this->model = new AdsRepository($ads);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data = [
            'adsItems'    => $this->model->all(),
            'categories'    => $this->model->getCategories()
        ];
        return view('adslist')->with($data);
    }

    /**
     * Store a newly created resource Note.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ads  $ads
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ads $ads)
    {
        $data = array(
            'status' => 1
        );
        $update = $this->model->update($data, $request->id);
        if ($update) {
            return redirect()->route('ads-list')->with('success', 'The Ad was deleted successfully.');
        } else {
            return redirect()->back()->with('error', 'Something went wrong. Please try again.');
        }

    }

    /**
     * Search.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ads  $ads
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $categoryId = ($request->cat !== null) ? $request->cat :  '0';
        $search = ($request->s !== null) ? $request->s : '';
        $data = [
            'adsItems'    => $this->model->search($categoryId, $search),
            'categories'    => $this->model->getCategories()
        ];
        return view('adslist')->with($data);
        // die($data->count());
        // $this->index($data);
    }

}
