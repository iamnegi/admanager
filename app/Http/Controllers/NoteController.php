<?php

namespace App\Http\Controllers;

use App\Note;
use Illuminate\Http\Request;
use App\Repositories\NoteRepository;

class NoteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $model;
    
    public function __construct(Note $note)
    {
        $this->middleware('auth');
        $this->model = new NoteRepository($note);
    }

    /**
     * Store a newly created resource Note.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Note $note)
    {
        $data = array(
            'user_id' => Auth()->user()->id,
            'ads_id' => $request->ads_id,
            'title' => $request->title
        );
        $store = $this->model->create($data);

        if ($store) {
            return redirect()->route('ads-list')->with('success', 'Note successfully added.');
        } else {
            return redirect()->back()->with('error', 'Something went wrong. Please try again.');
        }
    }

    /**
     * getNotes Notes By ID
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getNotes(Request $request)
    {
        $result = $this->model->getNotesByAdId($request->adsId);
        return response()->json($result); 
    }

    /**
     * updateNote Notes By ID
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateNote(Request $request)
    {
        $data = array(
            'title' => $request->note,
            'updated_at' => date('Y-m-d h:i:s')
        );
        $isUpdated = $this->model->update($data, $request->nid);
        $result = array();
        if($isUpdated) {
            $result = array (
                'status' => true,
                'message' => 'Note updated successfully'
            );
        } else {
            $result = array (
                'status' => false,
                'message' => 'Opps! somethin went wrong!'
            );
        }
        return response()->json($result); 
    }

}
