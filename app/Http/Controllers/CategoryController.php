<?php

namespace App\Http\Controllers;

use App\Category;
use App\AdsCategory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $model;
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource Note.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Category $category)
    {
        
        $data = array(
            'user_id' => Auth()->user()->id,
            'title' => $request->title
        );
        $store = $category->create($data);

        if ($store) {
            return redirect()->route('ads-list')->with('success', 'Category successfully added.');
        } else {
            return redirect()->back()->with('error', 'Something went wrong. Please try again.');
        }
    }

    /**
     * Assign category
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function assignCategory(Request $request, AdsCategory $adsCategory)
    {
        
        $dataStore = array();
        foreach($request->category as $cat) {
            $localS = array(
                'category_id' => $cat,
                'ads_id' => $request->ads_id,
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            );
            array_push($dataStore , $localS);
        }
        $store = AdsCategory::insert($dataStore);
        
        if ($store) {
            return redirect()->route('ads-list')->with('success', 'Category successfully Assigned.');
        } else {
            return redirect()->back()->with('error', 'Something went wrong. Please try again.');
        }
    }
}
